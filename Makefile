AS= nasm
ASFLAGS= -f bin

all: floppy.img

floppy.img: src/bootsec.asm
	$(AS) $(ASFLAGS) $< -o $@
